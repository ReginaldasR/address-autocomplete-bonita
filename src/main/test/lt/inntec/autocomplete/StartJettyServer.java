package lt.inntec.autocomplete;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;



/**
 * Created by Reginaldas on 5/27/2014.
 */
public class StartJettyServer {


    public static void main(String[] args) throws Exception {
        Server server = new Server(8088);

        ServletHandler servletHandler = new ServletHandler();

        ServletHolder servlet = new ServletHolder(new AddressComponentServlet());
        servlet.setInitParameter("wsdl", "http://localhost:8083/zumis-ws/service/addressWs?wsdl");
        servlet.setInitParameter("ws", "AddressWsService");
        servlet.setInitParameter("namespace", "http://zumis.lt/service");
        servletHandler.addServletWithMapping(servlet, "/address/*");

        ResourceHandler rsHandler = new ResourceHandler();
        rsHandler.setDirectoriesListed(true);
        rsHandler.setWelcomeFiles(new String[]{"index.html"});
        rsHandler.setResourceBase(".");

        HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[]{rsHandler, servletHandler});

        server.setHandler(handlers);

        server.start();
        server.join();
    }
}