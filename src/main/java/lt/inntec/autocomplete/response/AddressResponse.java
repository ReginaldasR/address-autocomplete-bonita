package lt.inntec.autocomplete.response;

import com.tieto.zumis.ws.entity.*;
import com.tieto.zumis.ws.service.IAddressWs;
import com.tieto.zumis.ws.service.ServiceInstantiator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.util.List;

/**
 * Created by Reginaldas on 5/28/2014.
 */
public class AddressResponse {

    private Logger log = LoggerFactory.getLogger(AddressResponse.class);
    private IAddressWs addressWs;

    public AddressResponse(String wsdl, String namespace, String ws) {
        try {
            addressWs = ServiceInstantiator.getServicePort(wsdl, namespace, ws, IAddressWs.class);
        } catch (MalformedURLException e) {
            log.error("Encountered while creating ws instance. StackTrace : {}", e);
        }
    }

    public List<ElderShipWBean> getElderships(HttpServletRequest request) {
        String value = request.getParameter("value");
        long municipality = Long.parseLong(request.getParameter("municipality"));
        return addressWs.elderShipList(municipality, value);
    }

    public List<DistrictWBean> getDistricts() {
        return addressWs.distrintsList();
    }

    public List<MunicipalityWBean> getMunicipalities(HttpServletRequest request) {
        String value = request.getParameter("value");
        long district = Long.parseLong(request.getParameter("district"));
        return addressWs.municipalitiesList(district, value);
    }

    public List<SettlementWBean> getSettlements(HttpServletRequest request) {
        String value = request.getParameter("value");
        String elderShip1 = request.getParameter("elderShip");
        Long elderShip = null;
        if (!elderShip1.isEmpty()) {
            elderShip = Long.parseLong(elderShip1);
        }
        long municipality = Long.parseLong(request.getParameter("municipality"));
        return addressWs.settlementList(municipality, elderShip, value);
    }

    public List<StreetWBean> getStreets(HttpServletRequest request) {
        String value = request.getParameter("value");
        long settlement = Long.parseLong(request.getParameter("settlement"));
        return addressWs.streetList(settlement, value);
    }

    public String validateAndGetFullAddress(HttpServletRequest request) {
        Long district = parseLongFromString(request, "district");
        Long municipality = parseLongFromString(request, "municipality");
        Long settlement = parseLongFromString(request,"settlement");
        Long eldership = parseLongFromString(request, "eldership");
        Long street = parseLongFromString(request, "street");
        String house = request.getParameter("house");
        String flat = request.getParameter("flat");

        log.info("Got house {} and flat {}", house, flat);
        ValidAddressWBean validAddressWBean = addressWs.validateAddress(district, municipality,
                eldership, settlement, street, house, flat);
        if (validAddressWBean.getIsValid()) {
            return validAddressWBean.getFullAddress();
        }
        return "None";
    }

    private Long parseLongFromString(HttpServletRequest request, String paramName) {
        try {
            log.info("Trying to parse long");
            return Long.parseLong(request.getParameter(paramName));
        } catch (Exception e) {
            log.info("Failed parse long for param {}", paramName);
            return null;
        }
    }
}
