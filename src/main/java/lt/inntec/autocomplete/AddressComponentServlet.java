package lt.inntec.autocomplete;


import com.google.gson.Gson;
import com.tieto.zumis.ws.entity.DistrictWBean;
import lt.inntec.autocomplete.response.AddressResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Reginaldas on 5/27/2014.
 */
public class AddressComponentServlet extends HttpServlet {

    private Logger log = LoggerFactory.getLogger(AddressComponentServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=UTF-8");
        resp.getWriter().write(dispatchRequest(request));
    }

    private String dispatchRequest(HttpServletRequest request) {
        Gson json = new Gson();
        log.info("Checking which response method should be used for request with params {}", json.toJson(request.getParameterMap()));
        String type = request.getParameter("type");
        String objectString = null;
        String wsdl = getServletConfig().getInitParameter("wsdl");
        String namespace = getServletConfig().getInitParameter("namespace");
        String ws = getServletConfig().getInitParameter("ws");
        AddressResponse addressResponse = new AddressResponse(wsdl, namespace, ws);

        if (type != null) {
            if (type.equals("district")) {
                List<DistrictWBean> districtWBeans = addressResponse.getDistricts();
                objectString = json.toJson(districtWBeans);
            } else if (type.equals("municipality")) {
                objectString = json.toJson(addressResponse.getMunicipalities(request));
            } else if (type.equals("elderShip")) {
                objectString = json.toJson(addressResponse.getElderships(request));

            } else if (type.equals("settlement")) {
                objectString = json.toJson(addressResponse.getSettlements(request));
            }
            else if (type.equals("street")) {
                objectString = json.toJson(addressResponse.getStreets(request));
            } else if (type.equals("validate")) {
                objectString = json.toJson((addressResponse.validateAndGetFullAddress(request)));
            }
        } else {
            log.warn("No parameters where passed for service, returning null");
            return null;
        }
        return objectString;
    }
}
