var municipalities = [];
var elderships = [];
var settlements = [];
var streets = [];

function realElderID() {
    if ($("#eldership").length > 0) {
        return $(".eldership").val();
    } else {
        return null;
    }
}

function initMunicipalities() {
    municipalities = [];
    $('#municipality').autocomplete({ source: municipalities });
    $.ajax({
        url: "address?type=municipality&district=" + $(".district").val(),
        data: "json",
        success: function (data) {
            $.each(data, function () {
                municipalities.push({value: this.id, label: this.name});
            });
            $('#municipality').autocomplete({ source: municipalities });
        }});
}

function initElderships() {
    elderships = [];
    $('#eldership').autocomplete({ source: elderships });
    $.ajax({
        url: "address?type=elderShip&municipality=" + $(".municipality").val(),
        data: "json",
        success: function (data) {
            $.each(data, function () {
                elderships.push({value: this.id, label: this.name});
            });
            $('#eldership').autocomplete({ source: elderships });
        }});
}

function initSettlements() {
    settlements = [];
    $('#settlement').autocomplete({ source: settlements });
    $.ajax({
        url: "address?type=settlement&elderShip=" + realElderID() + "&municipality=" + $(".municipality").val(),
        data: "json",
        success: function (data) {
            $.each(data, function () {
                settlements.push({value: this.id, label: this.name});
            });
            $('#settlement').autocomplete({ source: settlements });
        }});
}

function initStreets() {
    streets = [];
    $('#street').autocomplete({ source: streets });
    $.ajax({
        url: "address?type=street&settlement=" + $(".settlement").val(),
        data: "json",
        success: function (data) {
            $.each(data, function () {
                streets.push({value: this.id, label: this.name});
            });
            $('#street').autocomplete({ source: streets });
        }});
}

function initAutocomplete(district, lenght) {
    $("#district").autocomplete({
        minLength: lenght,
        source: district,
        open: function (event, ui) {
            $("#district").focus();
        },
        close: function (event, ui) {
            $("#district").autocomplete({minLength: 3});
        },
        focus: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
        },
        select: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $(".district").val(ui.item.value).change();
        },
        change: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $(".district").val(ui.item.value).change();
            if (ui.item != null) {
                initMunicipalities();
            }
        }
    });
}

function init() {
    var district = [];
    $.ajax({
        url: "address?type=district",
        data: "json",
        success: function (data) {
            $.each(data, function () {
                district.push({value: this.id, label: this.name});
            });
        }});

    initAutocomplete(district, 3);

    $("#districtButton").on("click", function () {
        $("#district").autocomplete({minLength: 0});
        $("#district").autocomplete("search", $("#district").val());

        $("#district").on("blur", function () {
            initAutocomplete(district, 3);
        })
    });

    $("#house").on("blur", function () {
        $(".house").val($("#house").val());
    });

    $("#flat").on("blur", function () {
        $(".flat").val($("#flat").val());
    });

    $("#address_no_LT").on("blur", function () {
        $(".address_no_LT").val($("#address_no_LT").val());
    });
}

$(document).on("change", function () {
    if ($(".district").length > 0) {
        $("#municipality").autocomplete({
            minLength: 3,
            source: municipalities,
            open: function (event, ui) {
                $("#municipality").focus();
            },
            close: function (event, ui) {
                $("#municipality").autocomplete({minLength: 3});
            },
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label).change();
                $(".municipality").val(ui.item.value).change();
            },
            change: function (event, ui) {
                event.preventDefault();
                if (ui.item == null) {
                    $(this).val('').change();
                } else {
                    $(this).val(ui.item.label).change();
                    $(".municipality").val(ui.item.value).change();
                    initSettlements();
                    initElderships();
                }
            }
        });

        $("#municipalityButton").on("click", function () {
            $("#municipality").autocomplete({minLength: 0});
            $("#municipality").autocomplete("search", $("#municipality").val());
        })
    }

    if ($("input[value='municipality']").length > 0) {
        $("#eldership").autocomplete({
            minLength: 3,
            source: elderships,
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                $(".eldership").val(ui.item.value).change();
            },
            change: function (event, ui) {
                event.preventDefault();
                if (ui.item == null) {
                    $(this).val('');
                } else {
                    $(this).val(ui.item.label);
                    $(".eldership").val(ui.item.value).change();
                    initSettlements();
                }
            }
        });

        $("#eldershipButton").on("click", function () {
            $("#eldership").autocomplete({minLength: 0});
            $("#eldership").autocomplete("search", $("#eldership").val());
        })
    }

    function realElderID() {
        if ($("#eldership").length > 0) {
            return $(".eldership").val();
        } else {
            return null;
        }
    }

    $("#settlement").autocomplete({
        minLength: 3,
        source: settlements,
        focus: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
        },
        select: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $(".settlement").val(ui.item.value).change();
        },
        change: function (event, ui) {
            event.preventDefault();
            if (ui.item == null) {
                $(this).val('');
            } else {
                $(this).val(ui.item.label);
                $(".settlement").val(ui.item.value).change();
                initStreets();
            }
        }
    });

    $("#settlementButton").on("click", function () {
        $("#settlement").autocomplete({minLength: 0});
        $("#settlement").autocomplete("search", $("#settlement").val());
    });

    if ($("#settlement").length > 0) {
        $("#street").autocomplete({
            minLength: 3,
            source: streets,
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                $(".street").val(ui.item.value).change();
            },
            change: function (event, ui) {
                event.preventDefault();
                if (ui.item == null) {
                    $(this).val('');
                } else {
                    $(this).val(ui.item.label);
                    $(".street").val(ui.item.value).change();
                }
            }
        });

        $("#streetButton").on("click", function () {
            $("#street").autocomplete({minLength: 0});
            $("#street").autocomplete("search", $("#street").val());
        })
    }
});


$(".district").on("change", (function () {
    $(".municipality").val('');
    $(".eldership").val('');
    $(".settlement").val('');
    $(".street").val('');
    $("#municipality").val('');
    $("#eldership").val('');
    $("#settlement").val('');
    $("#street").val('');
}));

$(".municipality").on("change", (function () {
    $(".eldership").val('');
    $(".settlement").val('');
    $(".street").val('');
    $("#eldership").val('');
    $("#settlement").val('');
    $("#street").val('');
}));

$(".eldership").on("change", (function () {
    $(".settlement").val('');
    $(".street").val('');
    $("#settlement").val('');
    $("#street").val('');
}));

$(".settlement").on("change", (function () {
    $(".street").val('');
    $("#street").val('');
}));

var counter = 0;

function loadInitialData() {
    try {
        $('').autocomplete();
    } catch (e) {
        counter++;
        if (counter < 10) {
            setInterval(loadInitialData(), 100);
        }
    }
    if (document.getElementById("district") == null) {
        counter++;
        if (counter < 10) {
            setInterval(loadInitialData(), 100);
        }
    } else {
        init();
    }
}

//if (typeof(Trigger) !== 'function') {
//    var Trigger = function () {
//    };
//}

//if (typeof(Trigger.prototype.caller) !== 'function') {
//    Trigger.prototype.caller = function () {
//        loadInitialData();
//    }
//} else {
//    var trigger = new Trigger();
//    trigger.caller();
//}