$(".custom-address").click(function(){

    var parent = $(this).parent().parent().parent();

    parent.find("label[class='checked']").first().removeClass("checked");

    var $input = $(this).find("input").first();
    var $label = $(this).find("label").first();
    $label.addClass("checked");

    if($input.val() === 'LT') {
        $('.no_LT').hide();
        $('no_LT_add').hide();
        $('.LT').show();
        $('.add').show();
    } else {
        $('.LT').hide();
        $('.add').hide();
        $('.no_LT').show();
        $('no_LT_add').show();
    }
});