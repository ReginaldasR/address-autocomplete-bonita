var municipalities = [];
var elderships = [];
var settlements = [];
var streets = [];

function realElderID() {
    if ($("#eldership").length > 0) {
        return $("input[name='eldership']").val();
    } else {
        return null;
    }
}

function initMunicipalities() {
    municipalities = [];
    $('#municipality').autocomplete({ source: municipalities });
    $.ajax({
        url: "/bonita/address?type=municipality&district=" + $("input[name='district']").val(),
        data: "json",
        success: function (data) {
            $.each(data, function () {
                municipalities.push({value: this.id, label: this.name});
            });
            $('#municipality').autocomplete({ source: municipalities });
        }});
}

function initElderships() {
    elderships = [];
    $('#eldership').autocomplete({ source: elderships });
    $.ajax({
        url: "/bonita/address?type=elderShip&municipality=" + $("input[name='municipality']").val(),
        data: "json",
        success: function (data) {
            $.each(data, function () {
                elderships.push({value: this.id, label: this.name});
            });
            $('#eldership').autocomplete({ source: elderships });
        }});
}

function initSettlements() {
    settlements = [];
    $('#settlement').autocomplete({ source: settlements });
    $.ajax({
        url: "/bonita/address?type=settlement&elderShip=" + realElderID() + "&municipality=" + $("input[name='municipality']").val(),
        data: "json",
        success: function (data) {
            $.each(data, function () {
                settlements.push({value: this.id, label: this.name});
            });
            $('#settlement').autocomplete({ source: settlements });
        }});
}

function initStreets() {
    streets = [];
    $('#street').autocomplete({ source: streets });
    $.ajax({
        url: "/bonita/address?type=street&settlement=" + $("input[name='settlement']").val(),
        data: "json",
        success: function (data) {
            $.each(data, function () {
                streets.push({value: this.id, label: this.name});
            });
            $('#street').autocomplete({ source: streets });
        }});
}

function initAutocomplete(district, lenght) {
    $("#district").autocomplete({
        minLength: lenght,
        source: district,
        open: function (event, ui) {
            $("#district").focus();
        },
        close: function (event, ui) {
            $("#district").autocomplete({minLength: 3});
        },
        focus: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
        },
        select: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $("input[name='district']").val(ui.item.value).change();
        },
        change: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $("input[name='district']").val(ui.item.value).change();
            if (ui.item != null) {
                initMunicipalities();
            }
        }
    });
}

function init() {
    var district = [];
    $.ajax({
        url: "/bonita/address?type=district",
        data: "json",
        success: function (data) {
            $.each(data, function () {
                district.push({value: this.id, label: this.name});
            });
        }});

    initAutocomplete(district, 3);

    $("#districtButton").on("click", function () {
        $("#district").autocomplete({minLength: 0});
        $("#district").autocomplete("search", $("#district").val());

        $("#district").on("blur", function () {
            initAutocomplete(district, 3);
        })
    })

    $("#house").on("blur", function () {
        $("input[name='house']").val($("#house").val());
    });

    $("#flat").on("blur", function () {
        $("input[name='flat']").val($("#flat").val());
    });
}

$(document).on("change", function () {
    if ($("input[name='district']").length > 0) {
        $("#municipality").autocomplete({
            minLength: 3,
            source: municipalities,
            open: function (event, ui) {
                $("#municipality").focus();
            },
            close: function (event, ui) {
                $("#municipality").autocomplete({minLength: 3});
            },
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label).change();
                $("input[value='municipality']").val(ui.item.value).change();
            },
            change: function (event, ui) {
                event.preventDefault();
                if (ui.item == null) {
                    $(this).val('').change();
                } else {
                    $(this).val(ui.item.label).change();
                    $("input[name='municipality']").val(ui.item.value).change();
                    initSettlements()
                    initElderships();
                }
            }
        });

        $("#municipalityButton").on("click", function () {
            $("#municipality").autocomplete({minLength: 0});
            $("#municipality").autocomplete("search", $("#municipality").val());
        })
    }

    if ($("input[value='municipality']").length > 0) {
        $("#eldership").autocomplete({
            minLength: 3,
            source: elderships,
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                $("input[name='eldership']").val(ui.item.value).change();
            },
            change: function (event, ui) {
                event.preventDefault();
                if (ui.item == null) {
                    $(this).val('');
                } else {
                    $(this).val(ui.item.label);
                    $("input[name='eldership']").val(ui.item.value).change();
                    initSettlements();
                }
            }
        });

        $("#eldershipButton").on("click", function () {
            $("#eldership").autocomplete({minLength: 0});
            $("#eldership").autocomplete("search", $("#eldership").val());
        })
    }

    function realElderID() {
        if ($("#eldership").length > 0) {
            return $("input[name='eldership']").val();
        } else {
            return null;
        }
    }

    $("#settlement").autocomplete({
        minLength: 3,
        source: settlements,
        focus: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
        },
        select: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $("input[name='settlement']").val(ui.item.value).change();
        },
        change: function (event, ui) {
            event.preventDefault();
            if (ui.item == null) {
                $(this).val('');
            } else {
                $(this).val(ui.item.label);
                $("input[name='settlement']").val(ui.item.value).change();
                initStreets();
            }
        }
    });

    $("#settlementButton").on("click", function () {
        $("#settlement").autocomplete({minLength: 0});
        $("#settlement").autocomplete("search", $("#settlement").val());
    });

    if ($("#settlement").length > 0) {
        $("#street").autocomplete({
            minLength: 3,
            source: streets,
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                $("input[name='street']").val(ui.item.value).change();
            },
            change: function (event, ui) {
                event.preventDefault();
                if (ui.item == null) {
                    $(this).val('');
                } else {
                    $(this).val(ui.item.label);
                    $("input[name='street']").val(ui.item.value).change();
                }
            }
        });

        $("#streetButton").on("click", function () {
            $("#street").autocomplete({minLength: 0});
            $("#street").autocomplete("search", $("#street").val());
        })
    }
});


$("input[name='district']").on("change", (function () {
    $("input[name='municipality']").val('');
    $("input[name='eldership']").val('');
    $("input[name='settlement']").val('');
    $("input[name='street']").val('');
    $("#municipality").val('');
    $("#eldership").val('');
    $("#settlement").val('');
    $("#street").val('');
}));

$("input[name='municipality']").on("change", (function () {
    $("input[name='eldership']").val('');
    $("input[name='settlement']").val('');
    $("input[name='street']").val('');
    $("#eldership").val('');
    $("#settlement").val('');
    $("#street").val('');
}));

$("input[name='eldership']").on("change", (function () {
    $("input[name='settlement']").val('');
    $("input[name='street']").val('');
    $("#settlement").val('');
    $("#street").val('');
}));

$("input[name='settlement']").on("change", (function () {
    $("input[name='street']").val('');
    $("#street").val('');
}));

function loadInitialData() {
    if (document.getElementById("district") == null) {
        loadInitialData();
    } else {
        init();
    }
}
if (typeof(Trigger) !== 'function') {
    var Trigger = function() {};
}

if (typeof(Trigger.prototype.caller) !== 'function') {
    Trigger.prototype.caller = function() {
        loadInitialData();
    }
} else {
    var trigger = new Trigger();
    trigger.caller();
}

$.each(data, function(i) {i.inputmask("yyyy-mm-dd")})