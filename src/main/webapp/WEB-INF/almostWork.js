function initAutocomplete(district, lenght) {
    $("#district").autocomplete({
        minLength: lenght,
        source: district,
        open: function (event, ui) {
            $("#district").focus();
        },
        close: function (event, ui) {
            $("#district").autocomplete({minLength: 3});
        },
        focus: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
        },
        select: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $("input[name='district']").val(ui.item.value).change();
        },
        change: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $("input[name='district']").val(ui.item.value).change();
        }
    });
}

function init () {
    var district = [];
    $.ajax({
        url: "/bonita/address?type=district",
        data: "json",
        success: function (data) {
            $.each(data, function () {
                district.push({value: this.id, label: this.name});
            });
        }});

    initAutocomplete(district, 3);

    $("#districtButton").on("click", function () {
        $("#district").autocomplete({minLength: 0});
        $("#district").autocomplete("search", $("#district").val());

        $("#district").on("blur", function () {
            initAutocomplete(district, 3);
        })
    })

    $("#house").on("blur", function() {
        $("input[name='house']").val($("#house").val());
    });

    $("#flat").on("blur", function() {
        $("input[name='flat']").val($("#flat").val());
    });
}

$(document).on("change", function () {
    if ($("input[name='district']").length > 0) {
        $("#municipality").autocomplete({
            minLength: 3,
            source: function (request, response) {
                $.ajax({
                    url: "/bonita/address?type=municipality&district=" + $("input[name='district']").val() + "&value=" + $("#municipality").val(),
                    dataType: "json",
                    success: function (data) {
                        response($.map(data, function (v, i) {
                            return{
                                label: v.name,
                                value: v.id
                            }
                        }));
                    }
                })
            },
            open: function (event, ui) {
                $("#municipality").focus();
            },
            close: function (event, ui) {
                $("#municipality").autocomplete({minLength: 3});
            },
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label).change();
                $("input[value='municipality']").val(ui.item.value).change();
            },
            change: function (event, ui) {
                event.preventDefault();
                if (ui.item == null) {
                    $(this).val('').change();
                } else {
                    $(this).val(ui.item.label).change();
                    $("input[name='municipality']").val(ui.item.value).change();
                }
            }
        });

        $("#municipalityButton").on("click", function () {
            $("#municipality").autocomplete({minLength: 0});
            $("#municipality").autocomplete("search", $("#municipality").val());
        })
    }

    if ($("input[value='municipality']").length > 0) {
        $("#eldership").autocomplete({
            minLength: 3,
            source: function (request, response) {
                $.ajax({
                    url: "/bonita/address?type=elderShip&municipality=" + $("input[name='municipality']").val() + "&value=" + $("#eldership").val(),
                    dataType: "json",
                    success: function (data) {
                        response($.map(data, function (v, i) {
                            return{
                                label: v.name,
                                value: v.id
                            }
                        }));
                    }
                })
            },
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                $("input[name='eldership']").val(ui.item.value).change();
            },
            change: function (event, ui) {
                event.preventDefault();
                if (ui.item == null) {
                    $(this).val('');
                } else {
                    $(this).val(ui.item.label);
                    $("input[name='eldership']").val(ui.item.value).change();
                }
            }
        });

        $("#eldershipButton").on("click", function () {
            $("#eldership").autocomplete({minLength: 0});
            $("#eldership").autocomplete("search", $("#eldership").val());
        })
    }

    function realElderID() {
        if ($("#eldership").length > 0) {
            return $("input[name='eldership']").val();
        } else {
            return null;
        }
    }

    $("#settlement").autocomplete({
        minLength: 3,
        source: function (request, response) {
            $.ajax({
                url: "/bonita/address?type=settlement&elderShip=" + realElderID() + "&municipality=" + $("input[name='municipality']").val() + "&value=" + $("#settlement").val(),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (v, i) {
                        return{
                            label: v.name,
                            value: v.id
                        }
                    }));
                }
            })
        },
        focus: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
        },
        select: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $("input[name='settlement']").val(ui.item.value).change();
        },
        change: function (event, ui) {
            event.preventDefault();
            if (ui.item == null) {
                $(this).val('');
            } else {
                $(this).val(ui.item.label);
                $("input[name='settlement']").val(ui.item.value).change();
            }
        }
    });

    $("#settlementButton").on("click", function () {
        $("#settlement").autocomplete({minLength: 0});
        $("#settlement").autocomplete("search", $("#settlement").val());
    });

    if ($("#settlement").length > 0) {
        $("#street").autocomplete({
            minLength: 3,
            source: function (request, response) {
                $.ajax({
                    url: "/bonita/address?type=street&settlement=" + $("input[name='settlement']").val() + "&value=" + $("#street").val(),
                    dataType: "json",
                    success: function (data) {
                        response($.map(data, function (v, i) {
                            return{
                                label: v.name,
                                value: v.id
                            }
                        }));
                    }
                })
            },
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                $("input[name='street']").val(ui.item.value).change();
            },
            change: function (event, ui) {
                event.preventDefault();
                if (ui.item == null) {
                    $(this).val('');
                } else {
                    $(this).val(ui.item.label);
                    $("input[name='street']").val(ui.item.value).change();
                }
            }
        });

        $("#streetButton").on("click", function () {
            $("#street").autocomplete({minLength: 0});
            $("#street").autocomplete("search", $("#street").val());
        })
    }
});


$("input[name='district']").on("change", (function(){
    $("input[name='municipality']").val('');
    $("input[name='eldership']").val('');
    $("input[name='settlement']").val('');
    $("input[name='street']").val('');
    $("#municipality").val('');
    $("#eldership").val('');
    $("#settlement").val('');
    $("#street").val('');
}));

$("input[name='municipality']").on("change", (function(){
    $("input[name='eldership']").val('');
    $("input[name='settlement']").val('');
    $("input[name='street']").val('');
    $("#eldership").val('');
    $("#settlement").val('');
    $("#street").val('');
}));

$("input[name='eldership']").on("change", (function(){
    $("input[name='settlement']").val('');
    $("input[name='street']").val('');
    $("#settlement").val('');
    $("#street").val('');
}));

$("input[name='settlement']").on("change", (function(){
    $("input[name='street']").val('');
    $("#street").val('');
}));