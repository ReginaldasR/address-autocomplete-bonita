function initAutocomplete(district, lenght) {
    $("#district").autocomplete({
        minLength: lenght,
        source: district,
        focus: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
        },
        select: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            var oldVal = $("#realDistrict").val();
            $("#realDistrict").val(ui.item.value);
            if (oldVal != $("#realDistrict").val()) {
                $("#municipality").val("");
                $("#realMunicipality").val("");
            }
        },
        change: function (event, ui) {
            event.preventDefault();
            if (ui.item == null) {
                var oldVal = $("#realDistrict").val();
                $(this).val('');
                $("#realDistrict").val('');
                if (oldVal != $("#realDistrict").val()) {
                    $("#municipality").val("");
                    $("#realMunicipality").val("");
                }
            } else {
                $(this).val(ui.item.label);
                var oldVal = $("#realDistrict").val();
                $("#realDistrict").val(ui.item.value);
                if (oldVal != $("#realDistrict").val()) {
                    $("#municipality").val("");
                    $("#realMunicipality").val("");
                }
            }
        }
    });
}

$(document).ready(function () {

    var district = [];
    $.ajax({
        url: "http://127.0.0.1:8080/address?type=district",
        data: "json",
        success: function (data) {
            $.each(data, function () {
                district.push({value: this.id, label: this.name});
            });
        }});

    initAutocomplete(district, 3);

    $("#showAll").on("click", function () {
        initAutocomplete(district, 0)

        $("#district").autocomplete("search", "");

        $("#district").on("blur", function () {
            initAutocomplete(district, 3);
        })
    })

});

$(document).on("change", function () {
    if ($("#realDistrict").length > 0) {
        $("#municipality").autocomplete({
            minLength: 3,
            source: function (request, response) {
                $.ajax({
                    url: "http://127.0.0.1:8080/address?type=municipality&district=" + $("#realDistrict").val() + "&value=" + $("#municipality").val(),
                    dataType: "json",
                    success: function (data) {
                        response($.map(data, function (v, i) {
                            console.log(v.id);
                            return{
                                label: v.name,
                                value: v.id
                            }
                        }));
                    }
                })
            },
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                var oldVal = $("#realDistrict").val();
                $("#realDistrict").val(ui.item.value);
                if (oldVal != $("#realDistrict").val()) {
                    $("#municipality").val("");
                    $("#realMunicipality").val("");
                }
                $("#realMunicipality").val(ui.item.value);
            },
            change: function (event, ui) {
                event.preventDefault();
                if (ui.item == null) {
                    $(this).val('');
                } else {
                    $(this).val(ui.item.label);
                    $("#realMunicipality").val(ui.item.value);
                }
            }
        });
    }

    if ($("#realMunicipality").length > 0) {
        $("#eldership").autocomplete({
            minLength: 3,
            source: function (request, response) {
                $.ajax({
                    url: "http://127.0.0.1:8080/address?type=elderShip&municipality=" + $("#realMunicipality").val() + "&value=" + $("#eldership").val(),
                    dataType: "json",
                    success: function (data) {
                        response($.map(data, function (v, i) {
                            return{
                                label: v.name,
                                value: v.id
                            }
                        }));
                    }
                })
            },
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                $("#realEldership").val(ui.item.value);
            },
            change: function (event, ui) {
                event.preventDefault();
                if (ui.item == null) {
                    $(this).val('');
                } else {
                    $(this).val(ui.item.label);
                    $("#realEldership").val(ui.item.value);
                }
            }
        });
    }

    function realElderID() {
        if ($("#eldership").length > 0) {
            return $("#realEldership").val()
        } else {
            return null;
        }
    }

    console.log($("#realEldership").val());
    $("#settlement").autocomplete({
        minLength: 3,
        source: function (request, response) {
            $.ajax({
                url: "http://127.0.0.1:8080/address?type=settlement&elderShip=" + realElderID() + "&municipality=" + $("#realMunicipality").val() + "&value=" + $("#settlement").val(),
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (v, i) {
                        return{
                            label: v.name,
                            value: v.id
                        }
                    }));
                }
            })
        },
        focus: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
        },
        select: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            $("#realSettlement").val(ui.item.value);
        },
        change: function (event, ui) {
            event.preventDefault();
            if (ui.item == null) {
                $(this).val('');
            } else {
                $(this).val(ui.item.label);
                $("#realSettlement").val(ui.item.value);
            }
        }
    });

    if ($("#settlement").length > 0) {
        $("#street").autocomplete({
            minLength: 3,
            source: function (request, response) {
                $.ajax({
                    url: "http://127.0.0.1:8080/address?type=street&settlement=" + $("#realSettlement").val() + "&value=" + $("#street").val(),
                    dataType: "json",
                    success: function (data) {
                        response($.map(data, function (v, i) {
                            return{
                                label: v.name,
                                value: v.id
                            }
                        }));
                    }
                })
            },
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            select: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                $("#realStreet").val(ui.item.value);
            },
            change: function (event, ui) {
                event.preventDefault();
                if (ui.item == null) {
                    $(this).val('');
                } else {
                    $(this).val(ui.item.label);
                    $("#realStreet").val(ui.item.value);
                }
            }
        });
    }
});
